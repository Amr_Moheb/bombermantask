﻿/*
 Parent class for pickup system that detect the pickup collission
 
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PickUpsBehaviour : MonoBehaviour {

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
    
    public abstract void PickUpEffect(Collider x);

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Collider>().tag == "player"|| other.gameObject.GetComponent<Collider>().tag == "PlayerEdge")
        {
            PickUpEffect(other);
            Destroy(this.gameObject);
        }
    }
}
