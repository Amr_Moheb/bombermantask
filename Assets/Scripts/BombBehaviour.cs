﻿/*
 
 script resposible for each bomb behaviour
 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBehaviour : MonoBehaviour {
    GameObject Player;
	public Player PlayerObject;
	public PickUps PickUpScript;
    float startTime;
    public float delay=3f;
    RaycastHit hit;
	public int bombRange=2;
	public bool RemoteControlBomb = false;
    float DistanceFromPlayer=0f;
    float MinDistanceFromPlayer;
    bool bombTiggered = false;
    int PlayersCounter = 0;
    GameObject HittedPlayer;
    // Use this for initialization
    void Start () {
        startTime = Time.time;
       

    }
	
	// Update is called once per frame
	void Update () {
        // enable collider when the player leave the bomb position
        if (Player!=null) {
            DistanceFromPlayer = Vector3.Distance(Player.gameObject.transform.position, this.transform.position);

            if (DistanceFromPlayer > MinDistanceFromPlayer)
            {
                this.transform.GetComponent<SphereCollider>().enabled = true;

            }
        }
 // start bomb effect if default bomb time finished or if bomb is trigerred by other bomb
        if (Time.time-startTime> delay||bombTiggered==true) {
            //forward ray
            Vector3 DIr = transform.TransformDirection(Vector3.forward);
            BombRay(DIr);
            //back ray
            Vector3 DIr1 = transform.TransformDirection(-Vector3.forward);
            BombRay(DIr1);
            //right ray
            Vector3 DIr2 = transform.TransformDirection(Vector3.right);
            BombRay(DIr2);
           //left ray
            Vector3 DIr3 = transform.TransformDirection(-Vector3.right);
            BombRay(DIr3);

            //check if two players dead at the same explosion
            if (PlayersCounter >= 2) {
                HittedPlayer.gameObject.GetComponent<ActivePlayer>().AllPlayersLoss();
                Debug.Log("Two players dead from the same explosion");
              
            } else if( PlayersCounter ==1) {
                //check if one player dead
                HittedPlayer.GetComponent<ActivePlayer>().ILoss();
            }
            //desable remote controll mode after first remot control bomb exploded
			if(RemoteControlBomb==true){
				PlayerObject.RemoteControlBomb = false;
			}
            //give the player a new bomb
			PlayerObject.bombCount++;
            // explosion
            Destroy(this.gameObject);
           
        }
  
    }
    void BombRay( Vector3 Dir) {
        //start ray cast to dedetect objects around bomb
        if (Physics.Raycast(transform.position, Dir, out hit, bombRange))
        {
            Debug.DrawRay(transform.position, Dir, Color.green);
          
            //ray didnt colide with stone
            if (hit.collider.tag != "stone")
            {    
                //ray  colide with player
                if (hit.collider.tag == "player")
                {
                    //record the player that in  range of explossion
                    HittedPlayer = hit.collider.gameObject;
                    PlayersCounter++;
                    
                  
                }
                else if (hit.collider.tag == "PlayerEdge")
                {
                    //handle case when bomb inside the player and ray cast cant detect player collider
                    HittedPlayer = hit.collider.gameObject.transform.parent.gameObject;
                    PlayersCounter++;
                 
                }
                else if (hit.collider.tag == "bomb")
                {
                    //trigger another bomb
                    hit.collider.gameObject.GetComponent<BombBehaviour>().bombTrigger();
                }
                else
                {
                    //if a pickup in range of explosion its destroied and generate another randome one
                    PickUpScript.GeneratePickUp(hit.collider.transform);
                    Destroy(hit.collider.gameObject);
                }
            }
        }

    }
    public void SetPlayer( GameObject Player) {
        //set the minimam Distance at which the bomb enabled its collission
        this.Player = Player;
        MinDistanceFromPlayer = this.gameObject.GetComponent<SphereCollider>().radius*this.gameObject.transform.lossyScale.x + this.Player.gameObject.GetComponent<SphereCollider>().radius* this.Player.gameObject.transform.lossyScale.x;
    }
    public void bombTrigger() {
        // external bomb trigger event
        Debug.Log("bomb triggered by bomb");
        bombTiggered = true;

    }
}
