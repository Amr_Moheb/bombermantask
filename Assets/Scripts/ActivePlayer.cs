﻿/*
 
 Player script adjustable for multi players 

 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivePlayer : MonoBehaviour {

	public 	string Up;
	public	string Down;
	public	string Left;
	public	string Right;
    public string BombKey;
    public	float Speed;
    public GameObject PickupObj;
    public GameObject BombPrefab;
	public GameObject UiManagerObject;
    PickUps PickUpScript;
    Player AP;
    UIManger UiMangerScript;
	Rigidbody rb;
	//Vector3 MVector;
	// Use this for initialization
	void Start ()
    {   // adjust time scale for replay 
		Time.timeScale=1;
		UiMangerScript = UiManagerObject.GetComponent<UIManger> ();
		PickUpScript = PickupObj.gameObject.GetComponent<PickUps> ();
		rb = this.gameObject.GetComponent<Rigidbody> ();
        // creat an instance of player class and initialize control buttons and keys
		AP = new Player (Up,Down,Left,Right,Speed,BombKey);
	}
	
	// Update is called once per frame
	void Update () {
        // moving the player
			rb.velocity = AP.UpdatePosition ();
        //rotate charachter
			transform.forward = AP.UpdateRotation ();
        // player action of release bomb here
		AP.playerAction (BombPrefab,this.transform.gameObject,AP, PickUpScript);
	}
	public void IncreaseSpeed(int IncreaseSpeedAmount){
        // enable Increase Speed pickup mode
        Speed = Speed + IncreaseSpeedAmount;
	}

	public void IncreaseBombCount(int IncreaseBombAmount){
        // enable Increase bomb count pickup mode
        AP.bombCount = AP.bombCount + IncreaseBombAmount;
	}
	public void ActiveRemteControlBomb(int remoteControlTime){
        // Activate remote controll pick up 
		AP.SetRemoteControlTimer (remoteControlTime);
	}
	public void ActiveLongRangeBomb(int LBRange){
        // Activate long range bomb mode
        AP.SetLongBombRange (LBRange);
	}
	public void ILoss(){
        //enable loss ui and replay button
		Debug.Log ("Iloss"+this.gameObject.name);
		UiMangerScript.PlayerLoss (this.gameObject.name);
		Time.timeScale=0;
	}
    public void AllPlayersLoss() {
        // handle ui in cae all players damged withthin one explossion
        UiMangerScript.AllPlayersLoss();
        Time.timeScale = 0;
    }
}
