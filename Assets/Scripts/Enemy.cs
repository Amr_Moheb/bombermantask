﻿/*
 
 Enemy parent class resposible for enemy movement and enemy vision
 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
	Vector3 EnemyMovementVector;
	RaycastHit hit;
	Vector3 Dir;
	public LayerMask mask =8;
	protected enum EnemyMovement
	{
		MoveUp,
		MoveDown,
		MoveRight,
		MoveLeft
	}
	// Use this for initialization
	void Start () {
	//	layer_mask	= LayerMask.GetMask( "player");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    //Movement
	protected void DoAction(EnemyMovement enemyDir,Rigidbody EnemyRB,float Speed){

		switch (enemyDir)
		{
		case EnemyMovement.MoveUp:
			EnemyMovementVector.z = Speed * Time.deltaTime;
			EnemyMovementVector.x = 0;
			EnemyRB.velocity = EnemyMovementVector;
			break;
		case  EnemyMovement.MoveDown:
			EnemyMovementVector.z = -Speed * Time.deltaTime;
			EnemyMovementVector.x = 0;
			EnemyRB.velocity = EnemyMovementVector;
			break;
		case EnemyMovement.MoveRight:
			EnemyMovementVector.x = Speed * Time.deltaTime;
			EnemyMovementVector.z = 0;
			EnemyRB.velocity = EnemyMovementVector;
			break;
		case  EnemyMovement.MoveLeft:
			EnemyMovementVector.x = -Speed * Time.deltaTime;
			EnemyMovementVector.z = 0;
			EnemyRB.velocity = EnemyMovementVector;
			break;
		}

	}
    //get the distance between enemy and obsticles in a spesific direction
	protected float GetDistance(EnemyMovement EnemyDir){
		if(EnemyDir==EnemyMovement.MoveUp){

		Dir = transform.TransformDirection(Vector3.forward);
		}else if(EnemyDir==EnemyMovement.MoveDown){

			Dir = transform.TransformDirection(-Vector3.forward);
		}else if(EnemyDir==EnemyMovement.MoveRight){

			Dir = transform.TransformDirection(Vector3.right);
		}else if(EnemyDir==EnemyMovement.MoveLeft){

			Dir = transform.TransformDirection(-Vector3.right);
		}
		if (Physics.Raycast(transform.position, Dir, out hit))
		{
			Debug.DrawRay(transform.position, Dir, Color.green);	
			if(hit.collider.tag!="player"&& hit.collider.tag != "PlayerEdge")
            {
			return hit.distance;
			}
		}
		return 100000f;
	}
}
