﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpLongRangeBomb : PickUpsBehaviour {
	public int BombRange=10;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public override void PickUpEffect(Collider other)
    {
        //enable the long range bomb mode
        other.gameObject.GetComponent<ActivePlayer>().ActiveLongRangeBomb(BombRange);

    }
}
