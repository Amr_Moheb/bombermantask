﻿/*
  AI system for each enemy with adjastable sittings
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : Enemy
{

    Rigidbody Rb;
    public float Speed = 60f;
    float UpDis;
    float DownDis;
    float RightDis;
    float LeftDis;
    float CreticalDist = 0.5f;
    float CurrntDirDist = 100f;
    EnemyMovement CurrentDir = EnemyMovement.MoveUp;
    int ArrayCounter = 0;
    EnemyMovement[] ApproviedEnemtyDirections;

    // Use this for initialization
    void Start()
    {
        ApproviedEnemtyDirections = new EnemyMovement[4];
        Rb = this.gameObject.GetComponent<Rigidbody>();
        DoAction(CurrentDir, Rb, Speed);

        CurrntDirDist = UpDis;
    }

    // Update is called once per frame
    void Update()
    {
        EnemyAIMovement();
    }
    void EnemyAIMovement()
    {
        // scanner
        UpDis = GetDistance(EnemyMovement.MoveUp);
        DownDis = GetDistance(EnemyMovement.MoveDown);
        RightDis = GetDistance(EnemyMovement.MoveRight);
        LeftDis = GetDistance(EnemyMovement.MoveLeft);
        //set the current direction
        CurrntDirDist = GetDistance(CurrentDir);
        //if collide with some thing in its path will decide to change its path
        if (CurrntDirDist < CreticalDist)
        {
            //check which pathes are open
            if (UpDis > CreticalDist)
            {
                ApproviedEnemtyDirections[ArrayCounter] = EnemyMovement.MoveUp;
                ArrayCounter++;

            }
            if (DownDis > CreticalDist)
            {
                ApproviedEnemtyDirections[ArrayCounter] = EnemyMovement.MoveDown;
                ArrayCounter++;
            }
            if (LeftDis > CreticalDist)
            {
                ApproviedEnemtyDirections[ArrayCounter] = EnemyMovement.MoveLeft;
                ArrayCounter++;
            }
            if (RightDis > CreticalDist)
            {
                ApproviedEnemtyDirections[ArrayCounter] = EnemyMovement.MoveRight;

            }
            //select a random path from open pathes
            SelectPath();
        }
        else
        {
            //contanue moving
            DoAction(CurrentDir, Rb, Speed);
        }

    }
    //select a randome path from all open pathes
    void SelectPath()
    {
        int RandomNum = Random.Range(0, ArrayCounter + 1);
        DoAction(ApproviedEnemtyDirections[RandomNum], Rb, Speed);
        CurrentDir = ApproviedEnemtyDirections[RandomNum];
        ArrayCounter = 0;
    }
    // kill the player when touch him
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Collider>().tag == "player")
        {
            other.gameObject.GetComponent<ActivePlayer>().ILoss();

        }
        else if (other.gameObject.GetComponent<Collider>().tag == "PlayerEdge")
        {

            other.gameObject.transform.parent.gameObject.GetComponent<ActivePlayer>().ILoss();
        }
    }
}
