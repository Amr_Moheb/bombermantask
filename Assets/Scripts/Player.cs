﻿/*
 players parent class resposible for general movement behaviour and bomb throgh implementation
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    Vector3 MovementVector;
	Vector3 RotationVector;
    string Up;
    string Down;
    string Left;
    string Right;
    float Speed;
	string BombKey;
	public bool BombSpowned=false;
	public int bombCount = 1;
	public bool RemoteControlBomb = false;
	int remoteControlTime=6;
	public bool IncreaseBombRange = false;
	int BombRange=2;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //constractor to set the controll buttons
	public  Player(string Up, string Down, string Left, string Right, float Speed,string BombKey) {
		
        MovementVector = new Vector3(0,0,0);
		RotationVector = new Vector3(0,0,0);
        this.Up = Up;
        this.Down = Down;
        this.Left = Left;
        this.Right = Right;
		this.Speed = Speed;
		this.BombKey = BombKey;

    }

    public Vector3 UpdatePosition() {
        // Update MovementVector Values

		if (Input.GetKey (Up)) {
			MovementVector.z = Speed * Time.deltaTime;
			RotationVector.z = 1f;
			RotationVector.x = 0f;
		}
		if (Input.GetKey (Down)) {
			MovementVector.z = -Speed * Time.deltaTime;
			RotationVector.z = -1f;
			RotationVector.x = 0f;
		}
		if (Input.GetKey (Right)) {
			MovementVector.x = Speed * Time.deltaTime;
			RotationVector.x = 1f;
			RotationVector.z = 0f;
		}
		if (Input.GetKey (Left)) {
			MovementVector.x = -Speed * Time.deltaTime;
			RotationVector.x = -1f;
			RotationVector.z = 0f;
		}
		if(!Input.GetKey (Up)&&!Input.GetKey (Down)){//&&!Input.GetKey (Right)&&!Input.GetKey (Left)) {
			MovementVector.z = 0;
		}
		if(!Input.GetKey (Right)&&!Input.GetKey (Left)){
			MovementVector.x = 0;
		}

        return MovementVector;
    }

    //return the player direction
	public Vector3 UpdateRotation(){
		return RotationVector;
	}

	public void SetRemoteControlTimer(int RCTimer)
    {
		remoteControlTime = RCTimer;
		RemoteControlBomb = true;
		bombCount = 1;
	}
	public void SetLongBombRange(int LBRange){
		BombRange = LBRange;
		IncreaseBombRange = true;
	}

    //throw bombs behaviour
	public void playerAction(GameObject BombPrefab,GameObject Player,Player PlayerObject,PickUps pickUpScript){
	// on bomb button clicked
		if (Input.GetKey (BombKey))
        {
            // desable dublicated bombs By default
			if (bombCount > 0 && BombSpowned == false) {
				BombSpowned = true;
                //create new bomb
				GameObject GeneratedBomb =	Instantiate (BombPrefab, Player.transform.position, Quaternion.identity);
                //decrement your amount of bomb
				bombCount--;

				BombBehaviour bombScript =	GeneratedBomb.gameObject.GetComponent<BombBehaviour> ();
				if (RemoteControlBomb == true) {
                    //setup remote control bomb
					bombScript.delay = remoteControlTime;
					bombScript.RemoteControlBomb = false;
					RemoteControlBomb = false;
				} 
			
				if(IncreaseBombRange==true){
                    //setup long range bomb
					bombScript.bombRange = BombRange;
					IncreaseBombRange = false;
				}
                //general bomb settings setup
                bombScript.PlayerObject = PlayerObject;
                bombScript.SetPlayer(Player); 
				bombScript.PickUpScript = pickUpScript;

			
			}
		}
        else {
			BombSpowned = false;
		}
	}
}
