﻿/*
 Pickups Manger Script that mange pickups random appearance in the scence
 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUps : MonoBehaviour {
	public GameObject[] PickUpsObjects;
	// Use this for initialization
	public void GeneratePickUp(Transform PickUpPos){
		//random generation 30% probability to generate pick Up here
		int RandomNum = Random.Range (0, PickUpsObjects.Length*3);
		if(RandomNum<PickUpsObjects.Length){
		GameObject GeneratedBomb=	Instantiate(PickUpsObjects[RandomNum], PickUpPos.position, Quaternion.identity);
	
		}
	}
}
