﻿/*
 UI mAnger script is alayer that dealing with UI elements
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManger : MonoBehaviour {
	public Text WinText;
	public GameObject firstPlayer;
	public GameObject SecondPlayer;
	public GameObject ReplayBtn;
	public int timer=180;
	float startTime=0;
	public Text TimerText;

	// Use this for initialization
	void Start () {
        //set timer start
		startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        //decrement timer every second
		if(Time.time-startTime>1){
			startTime = Time.time;
			TimerText.text =""+ timer+"second";
			if(timer<1){
				//end round
				Time.timeScale=0;
				ReplayBtn.SetActive (true);
			}
			timer--;
			
		}
	}
    // reload button
	public void Replay(){
		SceneManager.LoadScene (0);
	}
    // player loss handle
	public void PlayerLoss(string LosserName){
		ReplayBtn.SetActive (true);
		WinText.text=LosserName+"-->Loss";
		if (LosserName == firstPlayer.name) {
			WinText.text = LosserName + "-->Loss---" + SecondPlayer.name + "-->win";
		} else {
			WinText.text = LosserName + "-->Loss---" + firstPlayer.name + "-->win";
		}
	}
    //all players dead case handling
    public void AllPlayersLoss() {

        ReplayBtn.SetActive(true);
        WinText.text = "No Winner";

    }
}
