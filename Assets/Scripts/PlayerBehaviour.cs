﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {
    public GameObject BombPrefab;
    
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

        if (Input.GetKeyDown(KeyCode.Space)) {
            if (GameManger.PlayerOneSpownedBomb==false) {
                Instantiate(BombPrefab, this.transform.position, Quaternion.identity);
                GameManger.PlayerOneSpownedBomb = true;
            }
          
        }


    }
}
