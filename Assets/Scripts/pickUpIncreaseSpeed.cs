﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickUpIncreaseSpeed : PickUpsBehaviour {
	
	public int IncreaseSpeedAmount=20;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    public override void PickUpEffect(Collider other)
    {
        other.gameObject.GetComponent<ActivePlayer>().IncreaseSpeed(IncreaseSpeedAmount);
    }
}
