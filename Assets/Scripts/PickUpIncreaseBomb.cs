﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpIncreaseBomb : PickUpsBehaviour {
	public int IncreaseBombAmount;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public override void PickUpEffect(Collider other)
    {
        //enable the increasing bomb amount mode
        other.gameObject.GetComponent<ActivePlayer>().IncreaseBombCount(IncreaseBombAmount);

    }
}
